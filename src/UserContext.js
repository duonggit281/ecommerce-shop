import React from "react";
export const UserContext = React.createContext();



//1. Tạo file UserContext.js như file này để create 1 Context(đặt tên là UserContext) ra mới có để sử dụng!
//2. Đứng tại file App.js (hoặc bất cứ file cha nào bọc hết các component lại bằng thẻ <UserContext.Provider>) => các component nằm trong sẽ lấy được hết data trong Context!
//3. Đứng tại file cần lấy data từ Context, gọi Context ra bằng cách: const user = useContext(UserContext); 
//-trong đó useContext(gõ dòng này tự động import vào file đang đứng) là hook có sẵn trong react(làm chức năng Consumer để lấy data trong Context ra dùng)
//-UserContext(gõ đoạn lệnh này tự động nó sẽ import vào file đang đứng chú ý bên file tạo ra nó phải có export) là nội dung đã đưa lên Context 


//Đưa tổng qty hiển thị lên Cart ở Header
//1. Tạo file UserContext.js như file này để create 1 Context(đặt tên là UserContext) ra mới có để sử dụng!
//2. Tại file App bọc hết các Component con trong Provider
//3. Tại file Home.js 
    //-Gọi useContext như sau const user = useContext(UserContext) (auto import 2 thứ hook useContext và UserContext, nếu ko auto thì phải gõ)
    //-chỗ function addCart thì lấy totalQty đưa vào hàm getTotalProduct
//4. Tại file App.js viết function getTotalProduct lấy qty đã lấy bên file Home.js set vào state getQty
//5. Tại thẻ Provider đưa value vào 2 thứ: 1-hàm getTotalProduct: getTotalProduct, 2-getQty: getQty để qua Header lấy ra dùng
//6. Tại file Header viết hàm renderCart để hiển thị qty ra; khai báo goi vào useContext để lấy data ra => set Qty vào hiển thị ra!
             