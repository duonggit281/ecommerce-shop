import React from 'react';
import ReactDOM from 'react-dom/client';
import{
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from 'react-router-dom';
import App from './App';
import Account from './Components/Account';
import AddProduct from './Components/AddProduct';
import Blog from './Components/Blog/Blog';
import Detail from './Components/Blog/Detail';
import Cart from './Components/Cart';
import EditProduct from './Components/EditProduct';
import Home from './Components/Home';
import Member from './Components/Member';
import MyProduct from './Components/MyProduct';
import ProductDetail from './Components/ProductDetail';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route path='/' element={<Home/>} />
          <Route path='/blog/list' element={<Blog/>} />
          <Route path='/blog/detail/:id' element={<Detail/>} />
          <Route path='/login' element={<Member/>} />                   
          <Route path='/account/update' element={<Account/>} />                   
          <Route path='/account/add-product' element={<AddProduct/>} />                   
          <Route path='/account/my-product' element={<MyProduct/>} />                   
          <Route path='/account/my-product/edit/:id' element={<EditProduct/>} />                   
          <Route path='/product-detail/:id' element={<ProductDetail/>} />                   
          <Route path='/cart' element={<Cart/>} />                   
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();



