import Footer from "./Components/Layout/Footer";
import Header from "./Components/Layout/Header";
import MenuLeft from "./Components/Layout/MenuLeft";
import Home from "./Components/Home";
import { useState } from "react";
import { UserContext } from "./UserContext";

function App(props) {
  const [getQty, setQty] = useState("");
  function getTotalProduct(x) {
    localStorage.setItem("item1", JSON.stringify(x));
    setQty(x);
    console.log(getQty);
  }
  return (
    <>
      <UserContext.Provider
        value={{
          getTotalProduct: getTotalProduct,
          getQty: getQty,
        }}
      >
        <Header />
        <section>
          <div className="container">
            <div className="row">
              <MenuLeft />
              {props.children}
            </div>
          </div>
        </section>
        <Footer />
      </UserContext.Provider>
    </>
  );
}

export default App;
