import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function ProductDetail(props) {
  const [getData, setData] = useState("");
  const [getImg, setImg] = useState("");
  const [getX, setX] = useState("");
  const params = useParams();
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/product/detail/" + params.id)
      .then((res) => {
        setData(res.data.data);

        let image = JSON.parse(res.data.data.image);
        setImg(image);
        setX(image[0]);
      });
  }, []);

  function renderImg1() {
    if (getX) {
      return (
        <img
          src={
            "http://localhost/laravel/public/upload/user/product/" +
            getData.id_user +
            "/" +
            getX
          }
          alt=""
        />
      );
    }
  }

  function renderImg2() {
    if (getImg.length > 0) {
      return getImg.map((value, key) => {
        return (
          <a>
            <img
              className="item-list"
              id={value}
              onClick={handleClick}
              src={
                "http://localhost/laravel/public/upload/user/product/" +
                getData.id_user +
                "/" +
                value
              }
              alt=""
            />
          </a>
        );
      });
    }
  }

  function handleClick(e) {
    let getId = e.target.id;
    setX(getId);
  }

  return (
    <div className="col-sm-9 padding-right">
      <div className="product-details">
        <div className="col-sm-5">
          <div className="view-product">{renderImg1()}</div>
          <div
            id="similar-product"
            className="carousel slide"
            data-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="item active">{renderImg2()}</div>
            </div>
          </div>
        </div>
        <div className="col-sm-7">
          <div className="product-information">
            <h2>{getData.name}</h2>
            <p>ID: {getData.id}</p>
            <span>
              <span>US ${getData.price}</span>
              <label>Quantity:</label>
              <input type="text" defaultValue={3} />
              <button type="button" className="btn btn-fefault cart">
                <i className="fa fa-shopping-cart" />
                Add to cart
              </button>
            </span>
            <p>
              <b>Availability:</b> In Stock
            </p>
            <p>
              <b>Condition:</b> New
            </p>
            <p>
              <b>Brand:</b> {getData.id_brand}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ProductDetail;
