import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function EditProduct(props) {
  
  const [getCategory, setCategory] = useState([]);
  const [getBrand, setBrand] = useState([]);
  useEffect(() => {
    axios
    .get("http://localhost/laravel/public/api/category-brand")
    .then(res => {
      setCategory(res.data.category)
      setBrand(res.data.brand)
    })
  },[])
  function renderCategory() {
    if (getCategory.length > 0) {
      return getCategory.map((value, key) =>{
        return(
          <option key={value.id} value={value.id}>
            {value.category}
          </option>
        )
      })
    }
  }
  function renderBrand() {
    if (getBrand.length > 0) {
      return getBrand.map((value, key) => {
        return(
          <option key={value.id} value={value.id}>
            {value.brand}
          </option>
        )
      })
    }
  }

  
  const status = [
    {id: 3, name:"Status"},
    {id: 1, name:"New"},
    {id: 0, name:"Sale"},
  ];
  function renderStatus() {
    if (status.length > 0) {
      return status.map((value, key) => {
        return(
          <option key={value.id} value={value.id}>
            {value.name}
          </option>
        )
      })
    }
  }
  const [getId, setId] = useState("");
  function getIdClick(e) {
    let id = e.target.value
    setId(id)
  }
  function renderSale() {
    if (getId == 0) {
      return(
        <label>
          <input type="sale" placeholder="0" name="sale" onChange={handleInput} />
        </label>
      )
    }
  }
  const params = useParams();
  
  useEffect(() =>{
    let userData = JSON.parse(localStorage.getItem("token-auth"));
    if (userData) {
      let url = "http://localhost/laravel/public/api/user/product/" + params.id;
      let accessToken = userData.success.token;
      let config = {
        headers: {
          Authorization: "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      axios
        .get(url, config)
        .then(res => {
          console.log(res.data.data);
          setInputs({
            name: res.data.data.name,   
            price: res.data.data.price,   
            category: res.data.data.id_category,   
            brand: res.data.data.id_brand,      
            status: res.data.data.status,   
            sale: res.data.data.sale,   
            company: res.data.data.company_profile,   
            file: "",   
            detail: res.data.data.detail,  
          })
        })
    }
  },[])
  

  const [inputs, setInputs] = useState({
    name: "",   
    price: "",   
    category: "",   
    brand: "",   
    status: "",   
    sale: "",   
    company: "",   
    file: "",   
    detail: "",   
  });
  function handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs(state => ({...state,[nameInput]: value}))
  }
  const [getFile, setFile] = useState("")
  function handleFile(e) {
    const fileList = e.target.files;
    setFile(fileList);
  }
  
  

  function handleSubmit(e) {
    e.preventDefault();
    let errorSubmit = {};
    let flag = 1;
    if (inputs.name == "") {
      flag = 2;
      errorSubmit.name = "Vui lòng nhập Name!";
    }
    if (inputs.price == "") {
      flag = 2;
      errorSubmit.price = "Vui lòng nhập Price!";
    }
    if (inputs.category == "") {
      flag = 2;
      errorSubmit.category = "Vui lòng chọn Category!";
    }
    if (inputs.brand == "") {
      flag = 2;
      errorSubmit.brand = "Vui lòng chọn Brand!";
    }
    if (getId == 3) {
      flag = 2;
      errorSubmit.status = "Vui lòng chọn Status!";
    }
    if (getId == 0) {
      if (inputs.sale == "") {
        flag = 2;
        errorSubmit.sale = "Vui lòng nhập Sale!";
      }
    }
    if (inputs.company == "") {
      flag = 2;
      errorSubmit.company = "Vui lòng nhập Company_Profile!";
    }
    if (inputs.detail == "") {
      flag = 2;
      errorSubmit.detail = "Vui lòng nhập Detail!";
    }

    if (flag == 2) {
      setErrors(errorSubmit);
    } else{
      setErrors({});
    }
  }

  const [errors, setErrors] = useState({});
  function renderError() {
    if (Object.keys(errors).length > 0) {
        return Object.keys(errors).map((key, index) => {
          return(
            <li key={index}>
              {errors[key]}
            </li>
          )
        })      
    }
  }
  
  return(
    <div className="col-sm-6">
      <div className="signup-form">
        <h2 className="text-center">Edit Product!</h2>
        <form onSubmit={handleSubmit} enctype="multipart/form-data">
                    <input type="text" value={inputs.name} placeholder="Name" name="name" onChange={handleInput} />

                    <input type="price" value={inputs.price} placeholder="Price" name="price" onChange={handleInput} />

                    <div className="form-group">
                        <select value={inputs.category} className="custom-select" name="category" onChange={handleInput}>
                            <option>Please choose category</option>
                             {renderCategory()}  
                        </select>
                    </div>

                    <div className="form-group">
                        <select value={inputs.brand} className="custom-select" name="brand" onChange={handleInput}>
                            <option>Please choose brand</option>
                            {renderBrand()}  
                        </select>
                    </div>

                    <div className="form-group">
                        <select value={inputs.status} className="custom-select" name="status" onChange={handleInput} onClick={getIdClick}>
                            {renderStatus()}  
                        </select>
                    </div>
                    {renderSale()}  

                    <input type="profile" value={inputs.company} placeholder="Company profile" name="company" onChange={handleInput} />

                    <input type="file" placeholder="File" name="file" multiple onChange={handleFile} />{/*multiple: upload nhiều file */}

                    <textarea name="detail" value={inputs.detail} placeholder="Detail" rows="10" cols="30" onChange={handleInput}/>

                    <button type="submit" className="btn btn-default">Sigup</button>

                    {renderError()}  
                </form>
      </div>
    </div>
  );
}
export default EditProduct;