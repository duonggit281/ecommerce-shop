import { useEffect, useState } from "react";
import axios from "axios";

function Account(props) {
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
    pass: "",
    phone: "",
    address: "",
    file: "",
  });

  const [errors, setErrors] = useState("");

  const [getFile, setFile] = useState("");

  const [avatar, setAvatar] = useState("");

  const userData = JSON.parse(localStorage.getItem("token-auth"));

  useEffect(() => {
    setInputs({
      name: userData.Auth.name,
      email: userData.Auth.email,
      pass: userData.Auth.pass,
      phone: userData.Auth.phone,
      address: userData.Auth.address,
      file: userData.Auth.avatar,
    });
  }, []);

  function handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs((state) => ({ ...state, [nameInput]: value }));
  }

  function handleInputFile(e) {
    const file = e.target.files;

    let reader = new FileReader();
    reader.onload = (e) => {
      setAvatar(e.target.result);
      setFile(file[0]);
    };
    reader.readAsDataURL(file[0]);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", inputs.name);
    formData.append("email", inputs.email);
    formData.append("password", inputs.password);
    formData.append("phone", inputs.phone);
    formData.append("address", inputs.address);
    formData.append("avatar", avatar);

    let accessToken = userData.success.token;

    let config = {
      headers: {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
      },
    };

    let url =
      "http://localhost/laravel/public/api/user/update/" + userData.Auth.id;
    axios.post(url, formData, config).then((res) => {
      console.log(res);
      if (res.data.errors) {
        setErrors(res.data.errors);
      } else {
        alert("Register Success!");
        let Token_Auth = res.data;
        localStorage.setItem("token-auth", JSON.stringify(Token_Auth));
      }
    });
  }

  return (
    <div>
      <div className="col-sm-4">
        <div className="signup-form">
          <h2>User Update</h2>
          <form onSubmit={handleSubmit} enctype="multipart/form-data">
            <input
              type="text"
              name="name"
              value={inputs.name}
              onChange={handleInput}
            />
            <input
              type="text"
              name="email"
              value={inputs.email}
              onChange={handleInput}
            />
            <input type="password" name="password" onChange={handleInput} />
            <input
              type="text"
              name="phone"
              value={inputs.phone}
              onChange={handleInput}
            />
            <input
              type="text"
              name="address"
              value={inputs.address}
              onChange={handleInput}
            />
            <input type="file" name="avatar" onChange={handleInputFile} />
            <button type="submit" className="btn btn-default">
              Update
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default Account;
