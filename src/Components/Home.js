import axios from "axios";
import { useContext, useEffect } from "react";
import { useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../UserContext";

function Home(props) {
  const user = useContext(UserContext);
  console.log(user);

  const [getData, setData] = useState("");
  useEffect(() => {
    axios.get("http://localhost/laravel/public/api/product").then((res) => {
      setData(res.data.data);
    });
  }, []);

  function addCart(e) {
    let getId = e.target.id;
    let item = {};
    let x = 1;
    let totalQty = 0;
    let getLocal = localStorage.getItem("item");
    if (getLocal) {
      item = JSON.parse(getLocal);
      Object.keys(item).map((value, key) => {
        if (getId == value) {
          x = 2;
          item[value] += 1;
        }
        totalQty += item[value];
      });
    }
    if (x == 1) {
      item[getId] = 1;
      totalQty += 1;
    }

    user.getTotalProduct(totalQty);

    localStorage.setItem("item", JSON.stringify(item));
  }

  function renderData() {
    if (getData.length > 0) {
      return getData.map((value, key) => {
        return (
          <div className="col-sm-4">
            <div className="product-image-wrapper">
              <div className="single-products">
                <div className="productinfo text-center">
                  <img
                    src={
                      "http://localhost/laravel/public/upload/user/product/" +
                      value.id_user +
                      "/" +
                      JSON.parse(value.image)[0]
                    }
                    alt=""
                  />
                  <h2>{value.price}</h2>
                  <p>{value.name}</p>
                  <a href="#" className="btn btn-default add-to-cart">
                    <i className="fa fa-shopping-cart" />
                    Add to cart
                  </a>
                </div>
                <div className="product-overlay">
                  <div className="overlay-content">
                    <h2>{value.price}</h2>
                    <p>{value.name}</p>
                    <a
                      href="#"
                      className="btn btn-default add-to-cart"
                      id={value.id}
                      onClick={addCart}
                    >
                      <i className="fa fa-shopping-cart" />
                      Add to cart
                    </a>
                  </div>
                </div>
              </div>
              <div className="choose">
                <ul className="nav nav-pills nav-justified">
                  <li>
                    <a href="#">
                      <i className="fa fa-plus-square" />
                      Add to wishlist
                    </a>
                  </li>
                  <li>
                    <Link to={"/product-detail/" + value.id}>
                      <i className="fa fa-plus-square" />
                      Add to compare
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      });
    }
  }

  return (
    <div>
      <section>
        <div className="container">
          <div className="row">
            <div className="col-sm-9 padding-right">
              <div className="features_items">
                <h2 className="title text-center">Features Items</h2>
                {renderData()}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Home;
