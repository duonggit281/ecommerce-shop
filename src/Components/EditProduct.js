import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
function EditProduct(props) {
  const [getCategory, setCategory] = useState([]);
  const [getBrand, setBrand] = useState([]);
  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/category-brand")
      .then((res) => {
        setCategory(res.data.category);
        setBrand(res.data.brand);
      });
  }, []);
  function handleCategory() {
    if (getCategory.length > 0) {
      return getCategory.map((value, key) => {
        return (
          <option key={value.id} value={value.id}>
            {value.category}
          </option>
        );
      });
    }
  }
  function handleBrand() {
    if (getBrand.length > 0) {
      return getBrand.map((value, key) => {
        return (
          <option key={value.id} value={value.id}>
            {value.brand}
          </option>
        );
      });
    }
  }

  const status = [
    { id: 3, name: "Status" },
    { id: 1, name: "New" },
    { id: 0, name: "Sale" },
  ];
  function renderStatus() {
    if (status.length > 0) {
      return status.map((value, key) => {
        return (
          <option key={value.id} value={value.id}>
            {value.name}
          </option>
        );
      });
    }
  }
  const [getId, setId] = useState("");
  function handleClick(e) {
    let id = e.target.value;
    setId(id);
  }
  function renderSale() {
    if (getId == 0) {
      return (
        <label>
          <input
            type="sale"
            placeholder="0"
            name="sale"
            onChange={handleInput}
          />
        </label>
      );
    }
  }

  const [inputs, setInputs] = useState({
    name: "",
    price: "",
    category: "",
    brand: "",
    status: "",
    sale: "",
    company: "",
    file: "",
    detail: "",
  });
  function handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs((state) => ({ ...state, [nameInput]: value }));
  }

  const [data, setData] = useState("");
  const params = useParams();
  useEffect(() => {
    let userData = JSON.parse(localStorage.getItem("token-auth"));
    if (userData) {
      let url = "http://localhost/laravel/public/api/user/product/" + params.id;
      let accessToken = userData.success.token;
      let config = {
        headers: {
          Authorization: "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      axios.get(url, config).then((res) => {
        setData(res.data.data);
        setInputs({
          name: res.data.data.name,
          price: res.data.data.price,
          category: res.data.data.id_category,
          brand: res.data.data.id_brand,
          status: res.data.data.status,
          sale: res.data.data.sale,
          company: res.data.data.company_profile,
          detail: res.data.data.detail,
        });
      });
    }
  }, []);

  const [getFile, setFile] = useState("");
  function handleFile(e) {
    let fileList = e.target.files;
    setFile(fileList);
  }
  const [getImgCheck, setImgCheck] = useState([]);
  function handleCheckBox(e) {
    let nameImg = e.target.value;
    let check = e.target.checked;
    if (check) {
      setImgCheck((state) => [...state, nameImg]);
    } else {
      let filImg = getImgCheck.filter((item, i) => {
        return item != nameImg;
      });
      setImgCheck(filImg);
    }
  }

  function renderImage() {
    if (Object.keys(data).length > 0) {
      let renderImg = data.image;
      return renderImg.map((value, key) => {
        return (
          <li>
            <img
              className="image_edit"
              src={
                "http://localhost/laravel/public/upload/user/product/" +
                data.id_user +
                "/" +
                value
              }
              alt=""
            />
            <input
              type="checkbox"
              value={value}
              name="avatarCheckBox"
              onChange={handleCheckBox}
            />
          </li>
        );
      });
    }
  }

  function handleSubmit(e) {
    console.log(getImgCheck);
    e.preventDefault();
    let errorSubmit = {};
    let x = 1;

    if (inputs.name == "") {
      x = 2;
      errorSubmit.name = "Vui lòng không để trống name !";
    }
    if (inputs.price == "") {
      x = 2;
      errorSubmit.price = "Vui lòng không để trống price !";
    }
    if (inputs.category == "") {
      x = 2;
      errorSubmit.category = "Vui lòng không để trống category !";
    }
    if (inputs.brand == "") {
      x = 2;
      errorSubmit.brand = "Vui lòng không để trống brand !";
    }
    if (getId == 3) {
      x = 2;
      errorSubmit.status = "Vui lòng không để trống status!";
    }
    if (getId == 0) {
      if (inputs.sale == "") {
        x = 2;
        errorSubmit.sale = "Vui lòng không để trống sale!";
      }
    }
    if (inputs.company == "") {
      x = 2;
      errorSubmit.company = "Vui lòng không để trống company profile!";
    }
    if (inputs.detail == "") {
      x = 2;
      errorSubmit.detail = "Vui lòng không để trống detail!";
    }

    if (Object.keys(getFile).length > 0) {
      Object.keys(getFile).map((key, index) => {
        if (getFile.length > 3) {
          x = 2;
          errorSubmit.file = "Vui lòng không upload nhiều hơn 3 hình ảnh!";
        }
        if (getFile[key] > 1024 * 1024) {
          x = 2;
          errorSubmit.file =
            "Vui lòng upload file ảnh có dung lượng thấp hơn 1mb!";
        }
        let fileName = getFile[key].name;
        let fileNameSplit = fileName.split(".");
        let fileType = fileNameSplit[1];

        let imgFile = ["png", "jpg", "jpeg", "PNG", "JPG"];
        let imgCheck = imgFile.includes(fileType);

        if (!imgCheck) {
          x = 2;
          errorSubmit.file = "Vui lòng upload đúng định dạng file ảnh!";
        }
      });
    } else {
      x = 2;
      errorSubmit.file = "Vui lòng upload ảnh!";
    }

    const finalImg = parseInt(getImgCheck.length) + parseInt(getFile.length);
    if (finalImg > 3) {
      x = 2;
      errorSubmit.file = "Số lượng ảnh upload cuối cùng không được quá 3 ảnh!";
    }

    if (x == 2) {
      setErrors(errorSubmit);
    } else {
      setErrors({});
      let localToken = JSON.parse(localStorage.getItem("token-auth"));
      let getToken = localToken.success.token;
      let config = {
        headers: {
          Authorization: "Bearer " + getToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      let url =
        "http://localhost/laravel/public/api/user/edit-product/" + params.id;

      const formData = new FormData();
      formData.append("name", inputs.name);
      formData.append("price", inputs.price);
      formData.append("category", inputs.category);
      formData.append("brand", inputs.brand);
      formData.append("company", inputs.company);
      formData.append("detail", inputs.detail);
      formData.append("status", inputs.status);
      formData.append("sale", inputs.sale ? inputs.sale : 0);
      Object.keys(getFile).map((item, i) => {
        formData.append("file[]", getFile[item]);
      });
      Object.keys(getImgCheck).map((item, i) => {
        formData.append("avatarCheckBox[]", getImgCheck[item]);
      });
      axios.post(url, formData, config).then((res) => {
        console.log(res);
      });
    }
  }

  const [errors, setErrors] = useState({});
  function renderError() {
    if (Object.keys(errors).length > 0) {
      return Object.keys(errors).map((key, index) => {
        return <li key={index}>{errors[key]}</li>;
      });
    }
  }

  return (
    <div className="col-sm-6">
      <div className="signup-form">
        <h2 className="text-center">Edit Product!</h2>
        <form onSubmit={handleSubmit} enctype="multipart/form-data">
          <input
            type="text"
            value={inputs.name}
            name="name"
            onChange={handleInput}
          />

          <input
            type="price"
            value={inputs.price}
            name="price"
            onChange={handleInput}
          />

          <div className="form-group">
            <select
              value={inputs.category}
              className="custom-select"
              name="category"
              onChange={handleInput}
            >
              <option>Please choose category</option>
              {handleCategory()}
            </select>
          </div>

          <div className="form-group">
            <select
              value={inputs.brand}
              className="custom-select"
              name="brand"
              onChange={handleInput}
            >
              <option>Please choose brand</option>
              {handleBrand()}
            </select>
          </div>

          <div className="form-group">
            <select
              value={inputs.status}
              className="custom-select"
              name="status"
              onChange={handleInput}
              onClick={handleClick}
            >
              {renderStatus()}
            </select>
          </div>

          {renderSale()}

          <input
            type="profile"
            value={inputs.company}
            name="company"
            onChange={handleInput}
          />

          <input
            type="file"
            placeholder="File"
            name="file"
            multiple
            onChange={handleFile}
          />

          {renderImage()}

          <textarea
            name="detail"
            value={inputs.detail}
            rows="10"
            cols="30"
            onChange={handleInput}
          />

          <button type="submit" className="btn btn-default">
            Sigup
          </button>
          {renderError()}
        </form>
      </div>
    </div>
  );
}
export default EditProduct;
