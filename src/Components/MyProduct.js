import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

function MyProduct(props) {
  const [data, setData] = useState("");

  useEffect(() => {
    let userData = JSON.parse(localStorage.getItem("token-auth"));
    if (userData) {
      let url = "http://localhost/laravel/public/api/user/my-product";
      let accessToken = userData.success.token;
      let config = {
        headers: {
          Authorization: "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      axios.get(url, config).then((res) => {
        console.log(res.data.data);
        setData(res.data.data);
      });
    }
  }, []);

  function checkId(e) {
    let getId = e.target.value;
    let urlDelete =
      "http://localhost/laravel/public/api/user/delete-product/" + getId;
    let userData = JSON.parse(localStorage.getItem("token-auth"));
    let accessToken = userData.success.token;
    let config = {
      headers: {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
      },
    };
    axios.get(urlDelete, config).then((res) => {
      console.log(res.data.data);
      setData(res.data.data);
    });
  }

  function renderData() {
    return Object.keys(data).map((key, index) => {
      return (
        <tr key={data[key].id}>
          <td>{data[key].id}</td>
          <td>{data[key].name}</td>
          <td>
            <img
              className="image_product"
              src={
                "http://localhost/laravel/public/upload/user/product/" +
                data[key].id_user +
                "/" +
                JSON.parse(data[key].image)[0]
              }
              alt=""
            />
          </td>
          <td>${data[key].price}</td>
          <td>
            <Link to={"/account/my-product/edit/" + data[key].id}>Edit</Link>
            <button value={data[key].id} onClick={checkId}>
              X
            </button>
          </td>
        </tr>
      );
    });
  }
  return (
    <div className="col-sm-9">
      <div className="table-responsive cart_info">
        <table className="table table-condensed">
          <thead>
            <tr className="cart_menu">
              <td className="id">ID</td>
              <td className="name">Name</td>
              <td className="image">Image</td>
              <td className="price">Price</td>
              <td className="action">Action</td>
            </tr>
          </thead>
          <tbody>{renderData()}</tbody>
          <tfoot>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td className="addmore">
              <Link to="/account/add-product">Add New</Link>
            </td>
          </tfoot>
        </table>
      </div>
    </div>
  );
}
export default MyProduct;
