import axios from "axios";
import { useEffect, useState } from "react";

function Cart(props) {
  const [getData, setData] = useState([]);
  useEffect(() => {
    let getLocal = localStorage.getItem("item");
    if (getLocal) {
      getLocal = JSON.parse(getLocal);
      axios
        .post("http://localhost/laravel/public/api/product/cart", getLocal)
        .then((res) => {
          setData(res.data.data);
        });
    }
  }, []);

  function renderCart() {
    if (getData.length > 0) {
      return getData.map((value, key) => {
        let total = 0;
        let tong =
          parseInt(total) + parseInt(value.qty) * parseInt(value.price);
        return (
          <tr>
            <td className="cart_product">
              <a href>
                <img
                  className="image_product"
                  src={
                    "http://localhost/laravel/public/upload/user/product/" +
                    value.id_user +
                    "/" +
                    JSON.parse(value.image)[0]
                  }
                  alt=""
                />
              </a>
            </td>
            <td className="cart_description">
              <h4>
                <a href>{value.name}</a>
              </h4>
              <p>ID: {value.id}</p>
            </td>
            <td className="cart_price">
              <p>${value.price}</p>
            </td>
            <td className="cart_quantity">
              <div className="cart_quantity_button">
                <a
                  id={value.id}
                  onClick={clickSum}
                  className="cart_quantity_up"
                  href
                >
                  +
                </a>
                <input
                  className="cart_quantity_input"
                  type="text"
                  name="quantity"
                  defaultValue={1}
                  autoComplete="off"
                  size={2}
                  value={value.qty}
                />

                <a
                  id={value.id}
                  onClick={clickSub}
                  className="cart_quantity_down"
                  href
                >
                  -
                </a>
              </div>
            </td>
            <td className="cart_total">
              <p className="cart_total_price">${tong}</p>
            </td>
            <td className="cart_delete">
              <a
                id={value.id}
                onClick={clickDel}
                className="cart_quantity_delete"
                href
              >
                X
              </a>
            </td>
          </tr>
        );
      });
    }
  }

  function clickSum(e) {
    let getId = e.target.id;

    let newDataSum = [...getData];
    newDataSum.map((value, key) => {
      if (getId == value.id) {
        newDataSum[key].qty += 1;
      }
    });
    setData(newDataSum);

    let getLocal = localStorage.getItem("item");
    if (getLocal) {
      getLocal = JSON.parse(getLocal);
      Object.keys(getLocal).map((key, index) => {
        if (getId == key) {
          getLocal[key] += 1;
        }
      });
    }
    localStorage.setItem("item", JSON.stringify(getLocal));
  }
  //---------------------------------------------------------------------------------------------------------------------------------------------

  function clickSub(e) {
    let getId = e.target.id;
    let newDataSub = [...getData];
    newDataSub.map((value, key) => {
      if (getId == value.id) {
        if (value.qty > 1) {
          newDataSub[key].qty -= 1;
        } else {
          delete newDataSub[key];
        }
      }
    });

    newDataSub = newDataSub.filter((n) => n);
    console.log(newDataSub);

    setData(newDataSub);

    let getLocal = localStorage.getItem("item");
    if (getLocal) {
      getLocal = JSON.parse(getLocal);
      Object.keys(getLocal).map((key, index) => {
        if (getId == key) {
          if (getLocal[key] > 1) {
            getLocal[key] -= 1;
          } else {
            delete getLocal[getId];
          }
        }
      });
    }
    localStorage.setItem("item", JSON.stringify(getLocal));
  }

  function clickDel(e) {
    let getId = e.target.id;
    let newDataDel = [...getData];
    newDataDel.map((value, key) => {
      if (getId == value.id) {
        delete newDataDel[key];
      }
    });
    newDataDel = newDataDel.filter((n) => n);
    setData(newDataDel);
    let getLocal = localStorage.getItem("item");
    if (getLocal) {
      getLocal = JSON.parse(getLocal);
      delete getLocal[getId];
    }
    localStorage.setItem("item", JSON.stringify(getLocal));
  }

  function renderTotal() {
    if (getData.length > 0) {
      let totalAll = 0;

      getData.map((value, key) => {
        let tong = parseInt(getData[key].qty) * parseInt(getData[key].price);
        totalAll = parseInt(totalAll) + parseInt(tong);
      });

      return totalAll;
    }
  }

  return (
    <div className="col-sm-9">
      <section id="cart_items">
        <div className="container">
          <div className="breadcrumbs">
            <ol className="breadcrumb">
              <li>
                <a href="#">Home</a>
              </li>
              <li className="active">Shopping Cart</li>
            </ol>
          </div>

          <div className="table-responsive cart_info">
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="image">Item</td>
                  <td className="description" />
                  <td className="price">Price</td>
                  <td className="quantity">Quantity</td>
                  <td className="total">Total</td>
                  <td />
                </tr>
              </thead>
              <tbody>{renderCart()}</tbody>
            </table>
          </div>
        </div>
      </section>
      <section id="do_action">
        <div className="container">
          <div className="heading">
            <h3>What would you like to do next?</h3>
            <p>
              Choose if you have a discount code or reward points you want to
              use or would like to estimate your delivery cost.
            </p>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="chose_area">
                <ul className="user_option">
                  <li>
                    <input type="checkbox" />
                    <label>Use Coupon Code</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Use Gift Voucher</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Estimate Shipping &amp; Taxes</label>
                  </li>
                </ul>
                <ul className="user_info">
                  <li className="single_field">
                    <label>Country:</label>
                    <select>
                      <option>United States</option>
                      <option>Bangladesh</option>
                      <option>UK</option>
                      <option>India</option>
                      <option>Pakistan</option>
                      <option>Ucrane</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field">
                    <label>Region / State:</label>
                    <select>
                      <option>Select</option>
                      <option>Dhaka</option>
                      <option>London</option>
                      <option>Dillih</option>
                      <option>Lahore</option>
                      <option>Alaska</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field zip-field">
                    <label>Zip Code:</label>
                    <input type="text" />
                  </li>
                </ul>
                <a className="btn btn-default update" href>
                  Get Quotes
                </a>
                <a className="btn btn-default check_out" href>
                  Continue
                </a>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="total_area">
                <ul>
                  <li>
                    Cart Sub Total <span>$59</span>
                  </li>
                  <li>
                    Eco Tax <span>$2</span>
                  </li>
                  <li>
                    Shipping Cost <span>Free</span>
                  </li>
                  <li>
                    Total <span>${renderTotal()}</span>
                  </li>
                </ul>
                <a className="btn btn-default update" href>
                  Update
                </a>
                <a className="btn btn-default check_out" href>
                  Check Out
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Cart;
