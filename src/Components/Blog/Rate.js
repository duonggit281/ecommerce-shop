import { useEffect, useState } from "react";
import StarRatings from "react-star-ratings";
import axios from "axios";
import { useParams } from "react-router-dom";

function Rate(props) {
  const [rating, setRating] = useState(0);
  const [errors, setErrors] = useState("");
  const params = useParams();

  function changeRating(newRating, name) {
    setRating(newRating);
    let logined = JSON.parse(localStorage.getItem("loginOK"));
    if (logined) {
      const userData = JSON.parse(localStorage.getItem("token-auth"));

      let url = "http://localhost/laravel/public/api/blog/rate/" + params.id;

      let accessToken = userData.success.token;

      let config = {
        headers: {
          Authorization: "Bearer " + accessToken,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };

      const formData = new FormData();
      formData.append("user_id", userData.Auth.id);
      formData.append("blog_id", params.id);
      formData.append("rate", newRating);

      axios.post(url, formData, config).then((res) => {
        console.log(res);
      });
    } else {
      setErrors("Vui long dang nhap de Rate!");
    }
  }

  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/blog/rate/" + params.id)
      .then((res) => {
        console.log(res.data.data);

        let rateData = res.data.data;
        let lengthRate = rateData.length;
        console.log(lengthRate);

        let sum = 0;
        if (lengthRate > 0) {
          rateData.map((value, key) => {
            sum = sum + value.rate;
          });
          console.log(sum);
        }

        let tbc = sum / lengthRate;
        setRating(tbc);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return (
    <>
      <StarRatings
        rating={rating}
        starRatedColor="blue"
        changeRating={changeRating}
        numberOfStars={6}
        name="rating"
      />
      <p>{errors}</p>
    </>
  );
}

export default Rate;
