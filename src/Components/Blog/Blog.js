import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Blog(props) {
  const [getItem, setItem] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/blog")
      .then((res) => {
        setItem(res.data.blog.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  function fetchData() {
    if (Object.keys(getItem).length > 0) {
      return getItem.map((value, key) => {
        return (
          <div className="single-blog-post">
            <h3>{value.title}</h3>
            <div className="post-meta">
              <ul>
                <li>
                  <i className="fa fa-user" /> ID:{value.id}
                </li>
                <li>
                  <i className="fa fa-clock-o" /> Created_at: {value.created_at}
                </li>
                <li>
                  <i className="fa fa-calendar" /> Updated_at:{" "}
                  {value.updated_at}
                </li>
              </ul>
            </div>
            <a href>
              <img
                src={
                  "http://localhost/laravel/public/upload/Blog/image/" +
                  value.image
                }
                alt=""
              />
            </a>
            <p>
              {value.description}
              {value.content}
            </p>
            <Link to={"/blog/detail/" + value.id} className="btn btn-primary">
              Read More
            </Link>
          </div>
        );
      });
    }
  }
  return (
    <div className="col-sm-9">
      <div className="blog-post-area">
        <h2 className="title text-center">Latest From our Blog</h2>
        {fetchData()}
      </div>
    </div>
  );
}
export default Blog;
