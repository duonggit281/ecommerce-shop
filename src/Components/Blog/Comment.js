import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";

function Comment(props) {
  const params = useParams();

  const [comment, setComment] = useState("");
  const [errors, setErrors] = useState("");

  function handleInput(e) {
    setComment(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    let logined = JSON.parse(localStorage.getItem("loginOK"));
    if (logined) {
      if (comment == "") {
        setErrors("Vui long nhap binh luan!");
      } else {
        setErrors("");
        const userData = JSON.parse(localStorage.getItem("token-auth"));

        let url =
          "http://localhost/laravel/public/api/blog/comment/" + params.id;

        let accessToken = userData.success.token;

        let config = {
          headers: {
            Authorization: "Bearer " + accessToken,
            "Content-Type": "application/x-www-form-urlencoded",
            Accept: "application/json",
          },
        };
        const formData = new FormData();
        formData.append("id_blog", params.id);
        formData.append("id_user", userData.Auth.id);
        formData.append("id_comment", props.idReply ? props.idReply : 0);
        formData.append("comment", comment);
        formData.append("image_user", userData.Auth.avatar);
        formData.append("name_user", userData.Auth.name);
        axios.post(url, formData, config).then((res) => {
          console.log(res.data.data);
          props.getComment(res.data.data);
        });
      }
    } else {
      setErrors("Vui long dang nhap!");
    }
  }

  return (
    <div className="replay-box">
      <div className="row">
        <div className="col-sm-12">
          <h2>Leave a replay</h2>
          <div className="text-area">
            <div className="blank-arrow">
              <label>Your Name</label>
            </div>
            <span>*</span>
            <form onSubmit={handleSubmit}>
              <textarea
                name="message"
                rows={11}
                defaultValue={""}
                onChange={handleInput}
              />
              <p>{errors}</p>
              <button className="btn btn-primary">post comment</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Comment;
