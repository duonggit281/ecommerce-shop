import React from "react";

function ListComment(props) {
  function handleId(e) {
    let getId = e.target.id;

    props.getIdCha(getId);
  }

  function renderData() {
    let listComment = props.data;
    if (listComment.length > 0) {
      return listComment.map((value, key) => {
        if (parseInt(value.id_comment) == 0) {
          return (
            <React.Fragment key={key}>
              <li className="media">
                <a className="pull-left" href="#">
                  <img
                    className="media-object"
                    src={
                      "http://localhost/laravel/public/upload/user/avatar/" +
                      value.image_user
                    }
                    alt=""
                  />
                </a>
                <div className="media-body">
                  <ul className="sinlge-post-meta">
                    <li>
                      <i className="fa fa-user" />
                      {value.name_user}
                    </li>
                  </ul>
                  <p>{value.comment}</p>
                  <a
                    id={value.id}
                    onClick={handleId}
                    className="btn btn-primary"
                  >
                    <i className="fa fa-reply" />
                    Replay
                  </a>
                </div>
              </li>
              {listComment.map((value1, key1) => {
                console.log(key1);
                if (parseInt(value.id) == parseInt(value1.id_comment)) {
                  return (
                    <li key={key1} className="media second-media">
                      <a className="pull-left" href="#">
                        <img
                          className="media-object"
                          src={
                            "http://localhost/laravel/public/upload/user/avatar/" +
                            value1.image_user
                          }
                          alt=""
                        />
                      </a>
                      <div className="media-body">
                        <ul className="sinlge-post-meta">
                          <li>
                            <i className="fa fa-user" />
                            {value1.name_user}
                          </li>
                        </ul>
                        <p>{value1.comment}</p>
                        <a className="btn btn-primary" href>
                          <i className="fa fa-reply" />
                          Replay
                        </a>
                      </div>
                    </li>
                  );
                }
              })}
            </React.Fragment>
          );
        }
      });
    }
  }

  return (
    <div class="response-area">
      <h2>3 RESPONSES</h2>
      <ul class="media-list">{renderData()}</ul>
    </div>
  );
}
export default ListComment;
