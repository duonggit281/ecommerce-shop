import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Comment from "./Comment";
import Rate from "./Rate";
import ListComment from "./ListComment";
function Detail(props) {
  let params = useParams();

  const [data, setData] = useState("");

  const [comment, setComment] = useState([]);

  const [idReply, setIdReply] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost/laravel/public/api/blog/detail/" + params.id)
      .then((response) => {
        setData(response.data.data);
        setComment(response.data.data.comment);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  function fetchData() {
    return (
      <div className="single-blog-post">
        <h3>{data.title}</h3>
        <div className="post-meta">
          <ul>
            <li>
              <i className="fa fa-user" /> Mac Doe
            </li>
            <li>
              <i className="fa fa-clock-o" /> 1:33 pm
            </li>
            <li>
              <i className="fa fa-calendar" /> DEC 5, 2013
            </li>
          </ul>
        </div>
        <a href>
          <img
            src={
              "http://localhost/laravel/public/upload/Blog/image/" + data.image
            }
            alt=""
          />
        </a>
        <p>{data.description}</p> <br />
        <p>{data.content}</p> <br />
        <p>
          Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
          fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
          sequi nesciunt.
        </p>{" "}
        <br />
        <p>
          Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
          consectetur, adipisci velit, sed quia non numquam eius modi tempora
          incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
        </p>
      </div>
    );
  }

  function getComment(x) {
    let linkComment = comment.concat(x);
    setComment(linkComment);
  }

  function getIdCha(a) {
    setIdReply(a);
  }
  console.log(idReply);

  return (
    <div className="col-sm-9">
      <div className="blog-post-area">
        <h2 className="title text-center">Latest From our Blog </h2>
        {fetchData()}
      </div>
      <Rate />
      <ListComment data={comment} getIdCha={getIdCha} />
      <Comment getComment={getComment} idReply={idReply} />
    </div>
  );
}
export default Detail;
