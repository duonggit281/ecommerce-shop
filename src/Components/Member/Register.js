import axios from "axios";
import { useState } from "react";

function Register(props) {
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    address: "",
    file: "",
  });

  const [errors, setErrors] = useState({});

  const [getFile, setFile] = useState("");

  const [avatar, setAvatar] = useState("");

  function handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs((state) => ({ ...state, [nameInput]: value }));
  }

  function handleInputFile(e) {
    const file = e.target.files;

    let reader = new FileReader();
    reader.onload = (e) => {
      setAvatar(e.target.result);
      setFile(file[0]);
    };
    reader.readAsDataURL(file[0]);
  }

  function handleSubmit(e) {
    e.preventDefault();
    let errorSubmit = {};
    let x = 1;
    if (inputs.name == "") {
      x = 2;
      errorSubmit.name = "Enter your name!";
    }
    if (inputs.email == "") {
      x = 2;
      errorSubmit.email = "Enter your email!";
    }
    if (inputs.password == "") {
      x = 2;
      errorSubmit.password = "Enter your password!";
    }
    if (inputs.phone == "") {
      x = 2;
      errorSubmit.phone = "Enter your phone!";
    }
    if (inputs.address == "") {
      x = 2;
      errorSubmit.address = "Enter your address!";
    }

    if (getFile) {
      console.log(getFile);
      if (getFile.size > 1024 * 1024) {
        x = 2;
        errorSubmit.file = "Your file > 1mb!";
      }

      const fileName = getFile.name;
      const fileNameSplit = fileName.split(".");
      const fileType = fileNameSplit[1];
      const imgFile = ["png", "jpg", "jpeg", "PNG", "JPG"];
      const imgCheck = imgFile.includes(fileType);
      if (!imgCheck) {
        x = 2;
        errorSubmit.file = "Your file is not image!";
      }
    } else {
      x = 2;
      errorSubmit.file = "Upload your file!";
    }

    if (x == 2) {
      setErrors(errorSubmit);
    } else {
      setErrors({});

      const data = {
        name: inputs.name,
        email: inputs.email,
        password: inputs.password,
        phone: inputs.phone,
        address: inputs.address,
        avatar: avatar,
        level: 0,
      };
      axios
        .post("http://localhost/laravel/public/api/register", data)
        .then((res) => {
          console.log(res);
          if (res.data.errors) {
            setErrors(res.data.errors);
          } else {
            alert("Register Success!");
          }
        });
    }
  }

  function renderError() {
    if (Object.keys(errors).length > 0) {
      return Object.keys(errors).map((key, index) => {
        return <li key={index}>{errors[key]}</li>;
      });
    }
  }

  return (
    <div>
      <div className="col-sm-4">
        <div className="signup-form">
          <h2>New User Signup!</h2>
          <form onSubmit={handleSubmit} enctype="multipart/form-data">
            <input
              type="text"
              name="name"
              placeholder="Name..."
              onChange={handleInput}
            />
            <input
              type="text"
              name="email"
              placeholder="Email..."
              onChange={handleInput}
            />
            <input
              type="password"
              name="password"
              placeholder="Password..."
              onChange={handleInput}
            />
            <input
              type="text"
              name="phone"
              placeholder="Phone..."
              onChange={handleInput}
            />
            <input
              type="text"
              name="address"
              placeholder="Address..."
              onChange={handleInput}
            />
            <input type="file" name="avatar" onChange={handleInputFile} />
            <button type="submit" className="btn btn-default">
              Signup
            </button>
          </form>
          {renderError()}
        </div>
      </div>
    </div>
  );
}
export default Register;
