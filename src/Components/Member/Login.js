import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Login(props) {
  const [inputs, setInputs] = useState({
    email: "",
    password: "",
  });

  const [errors, setErrors] = useState({});

  const navigate = useNavigate();

  function handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs((state) => ({ ...state, [nameInput]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    let errorSubmit = {};
    let x = 1;

    if (inputs.email == "") {
      x = 2;
      errorSubmit.email = "Enter Email!";
    }
    if (inputs.password == "") {
      x = 2;
      errorSubmit.password = "Enter Password";
    }

    if (x == 2) {
      setErrors(errorSubmit);
    } else {
      setErrors({});
      const data = {
        email: inputs.email,
        password: inputs.password,
        level: 0,
      };
      axios
        .post("http://localhost/laravel/public/api/login", data)
        .then((res) => {
          console.log(res);
          if (res.data.errors) {
            setErrors(res.data.errors);
          } else {
            alert("Login Successful!");
            navigate("/");

            const loginSuccess = true;
            localStorage.setItem("loginOK", JSON.stringify(loginSuccess));

            let Token_Auth = res.data;
            localStorage.setItem("token-auth", JSON.stringify(Token_Auth));
          }
        });
    }
  }

  function renderError() {
    if (Object.keys(errors).length > 0) {
      return Object.keys(errors).map((key, index) => {
        return <li key={index}>{errors[key]}</li>;
      });
    }
  }

  return (
    <div className="col-sm-4 col-sm-offset-1">
      <div className="login-form">
        <h2>Login to your account</h2>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Email"
            name="email"
            value={inputs.email}
            onChange={handleInput}
          />
          <input
            type="password"
            placeholder="Password"
            name="password"
            value={inputs.password}
            onChange={handleInput}
          />
          <span>
            <input type="checkbox" className="checkbox" />
            Keep me signed in
          </span>
          <button type="submit" className="btn btn-default">
            Login
          </button>
        </form>
        {renderError()}
      </div>
    </div>
  );
}
export default Login;
